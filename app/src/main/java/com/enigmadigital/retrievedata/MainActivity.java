package com.enigmadigital.retrievedata;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;

import com.enigmadigital.retrievedata.calllog.Call;
import com.enigmadigital.retrievedata.calllog.CallsProvider;
import com.enigmadigital.retrievedata.contacts.Contact;
import com.enigmadigital.retrievedata.contacts.ContactsProvider;
import com.enigmadigital.retrievedata.core.Data;
import com.enigmadigital.retrievedata.telephony.Sms;
import com.enigmadigital.retrievedata.telephony.TelephonyProvider;
import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity implements OnClickListener {
    String TAG = "RetrievalActivity";
    Button buttonStart;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonStart = (Button) findViewById(R.id.buttonStart);

        buttonStart.setOnClickListener(this::onClick);

        String[] permissions = {Manifest.permission.READ_SMS, Manifest.permission.READ_CONTACTS, Manifest.permission.READ_CALL_LOG};
        if(!hasPermissions(this, permissions)){
            ActivityCompat.requestPermissions(this, permissions, 1);
        }
    }

    private boolean hasPermissions(Context context, String[] permissions)
    {
        for (int i = 0; i < permissions.length; i++) {
            int permissionCheck = ContextCompat.checkSelfPermission(context, permissions[i]);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    private void StartProcess()
    {
        String filenameSms = "ListSms";
        String filenameContacts = "ListContacts";
        String filenameCalls = "ListCalls";

        //Get SMS
        Log.d(TAG, "Get SMS");
        TelephonyProvider telephonyProvider = new TelephonyProvider(this);
        Data<Sms> lstSms = telephonyProvider.getSms(TelephonyProvider.Filter.INBOX);
        String jsonSms = new Gson().toJson(lstSms.getList());

        //final file path : /data/user/0/com.enigmadigital.retrievedata/files/ListSms
        try (FileOutputStream fos =  this.openFileOutput(filenameSms, Context.MODE_PRIVATE)) {
            fos.write(jsonSms.getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Get contacts
        Log.d(TAG, "Get Contacts");
        ContactsProvider contactsProvider = new ContactsProvider(this);
        Data<Contact> lstContacts = contactsProvider.getContacts();
        String jsonContacts = new Gson().toJson(lstContacts.getList());

        //final file path : /data/user/0/com.enigmadigital.retrievedata/files/ListContacts
        try (FileOutputStream fos =  this.openFileOutput(filenameContacts, Context.MODE_PRIVATE)) {
            fos.write(jsonContacts.getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Get call history
        Log.d(TAG, "Get Call history");
        CallsProvider callssProvider = new CallsProvider(this);
        Data<Call> lstCalls = callssProvider.getCalls();
        String jsonCalls = new Gson().toJson(lstCalls.getList());

        //final file path : /data/user/0/com.enigmadigital.retrievedata/files/ListContacts
        try (FileOutputStream fos =  this.openFileOutput(filenameCalls, Context.MODE_PRIVATE)) {
            fos.write(jsonCalls.getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onClick(View src) {
        switch (src.getId()) {
            case R.id.buttonStart:
                Log.d(TAG, "START");
                StartProcess();
                Log.d(TAG, "END");
                onDestroy();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                for (int i = 0; i < permissions.length; i++) {
                    String permission = permissions[i];
                    int grantResult = grantResults[i];

                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        Log.d(TAG, "permission granted for " + permission);
                    } else {
                        // permission denied
                        Log.d(TAG, "Permission denied to read your External storage");
                    }
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        int id= android.os.Process.myPid();
        android.os.Process.killProcess(id);
    }
}