package com.enigmadigital.retrievedata.telephony;

import android.annotation.TargetApi;
import android.net.Uri;
import android.os.Build;
import android.provider.Telephony.Sms.Conversations;
import android.provider.Telephony.TextBasedSmsColumns;

import com.enigmadigital.retrievedata.core.Entity;
import com.enigmadigital.retrievedata.core.FieldMapping;
import com.enigmadigital.retrievedata.core.IgnoreMapping;

@TargetApi(Build.VERSION_CODES.KITKAT)
public class Conversation extends Entity {

    @IgnoreMapping
    public static Uri uri = Conversations.CONTENT_URI;

    @FieldMapping(columnName = TextBasedSmsColumns.THREAD_ID, physicalType = FieldMapping.PhysicalType.Int)
    public int threadId;

    @FieldMapping(columnName = Conversations.MESSAGE_COUNT, physicalType = FieldMapping.PhysicalType.Int)
    public int messageCount;

    @FieldMapping(columnName = Conversations.SNIPPET, physicalType = FieldMapping.PhysicalType.String)
    public String snippet;
}
